"""
catch and replace every line no. of bug report
"""
import re
import random
from insert_comment import read_source_file, write_source_file


DISTANCE = 5
MAX_LINE = None


def modifier(original_line_no):
    global MAX_LINE
    diff = random.randint(-DISTANCE, DISTANCE)
    try:
        new_line_no = int(original_line_no.group(1)) + diff
        new_line_no = max(new_line_no, 0)
        new_line_no = min(new_line_no, MAX_LINE)
    except ValueError:
        return original_line_no.group(0)
    return '"Line": %d,' % new_line_no


def main():
    nonlocal MAX_LINE
    content = read_source_file('.\\test_data\\csa_report120.json')
    MAX_LINE = len(content.splitlines())
    new_content = re.sub(r'"Line": (\d+),', modifier, content)
    write_source_file('.\\test_data\\cas_report120_new.json', new_content)


if __name__ == '__main__':
    main()
