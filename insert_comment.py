"""
randomly insert comments,
    : only accept a pre-processed source code, it cannot handle nested comments
    : gcc -fpreprocessed -dD -E test.c
"""

import re
import random

CODE_PIECE = ' /*** Sourcebrella ***/'


def read_source_file(file_name):
    with open(file_name, encoding='utf-8') as f:
        return f.read()


def write_source_file(file_name, content):
    with open(file_name, 'w', encoding='utf-8') as f:
        f.write(content)


def match_gen(source_code):
    """
    count all spaces related characters
    :param source_code:
    :return:

    """
    matches = re.finditer(r'(\s+|\n)', source_code)
    return reversed(list(matches))


def main():
    source_code = read_source_file('.\\test_data\\CWE590_Free_Memory_Not_on_Heap__delete_array_char_static_82a.cpp')
    for m in match_gen(source_code=source_code):
        if random.randint(0, 100) > 60:
            source_code = '%s%s %s %s' % \
                          (source_code[:m.start(1)], m.group(1), CODE_PIECE, source_code[m.end(1):])
    print(source_code)


if __name__ == '__main__':
    main()
