
import os
import time
import subprocess


PCLI_PATH = '/bigdata/tmp/pinpoint/bin/pcli'


def get_tags():
    p = subprocess.Popen(['git', 'tag'], stdout=subprocess.PIPE)
    out, _ = p.communicate()
    return out.decode().splitlines()


if __name__ == '__main__':
    os.chdir('cJSON')
    for tag in get_tags():
        os.system('git checkout .')
        os.system('git checkout %s' % tag)  # change to `git checkout HEAD~1` to scan every single commit
        time.sleep(2)  # give me chance to Ctrl+C  -_-!
        os.system('%s run ../platform.json' % PCLI_PATH)
